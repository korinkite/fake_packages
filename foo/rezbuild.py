#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""A fake build system that actually does basically nothing."""
import os
import shutil
import sys


def build(source_path, install_path):
    """Build the package."""
    if not os.path.isdir(install_path):
        os.makedirs(install_path)

    for item in {
        "file.py",
    }:
        shutil.copy2(os.path.join(source_path, item), os.path.join(install_path, item))


if __name__ == "__main__":
    build(
        source_path=os.environ["REZ_BUILD_SOURCE_PATH"],
        install_path=os.environ["REZ_BUILD_INSTALL_PATH"],
    )

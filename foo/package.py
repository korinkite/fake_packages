#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A fake, "client" Rez package that is used for testing."""

name = "foo"

version = "0.1.0"

build_command = "python {root}/rezbuild.py {install}"

requires = ["bar-0+"]

tests = {
    "unittests": "python -m unittest discover",
}


def commands():
    """Do nothing."""

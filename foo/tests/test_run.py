#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Test `foo` for issues.

The idea is that this test case may succeed or fail based on if bar's package runs incorrectly.

"""

import unittest

from bar import some_module


class Test(unittest.TestCase):
    """A unittest of some kind."""

    def test_thing(self):
        """Some test that should normally run successfully."""
        some_module.some_command()

        self.assertTrue(True)
